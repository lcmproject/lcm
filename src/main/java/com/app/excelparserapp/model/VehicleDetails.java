package com.app.excelparserapp.model;

public class VehicleDetails {

	private long vehicleId;
	private String vehicleType;
	private String vehicleNumber;
	private String fleetType;
	private VendorDetails vendorDetails;
	
	public VendorDetails getVendorDetails() {
		return vendorDetails;
	}
	public void setVendorDetails(VendorDetails vendorDetails) {
		this.vendorDetails = vendorDetails;
	}
	public long getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getFleetType() {
		return fleetType;
	}
	public void setFleetType(String fleetType) {
		this.fleetType = fleetType;
	}


}
