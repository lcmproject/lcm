package com.app.excelparserapp.model;

import java.util.Date;

public class VehicleTransactions {


	private long transactionId;
	private Date vehicleArrivalTime;
	private double startKm;
	private double endKm;
	private double actKm;
	private int noOfHU;
	private Date vehicleReportingTime;
	private String attendanceTime;
	private double km;
	private double tat;
	private VehicleDetails vehicleDetails;

	
	public VehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}
	public void setVehicleDetails(VehicleDetails vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public Date getVehicleArrivalTime() {
		return vehicleArrivalTime;
	}
	public void setVehicleArrivalTime(Date vehicleArrivalTime) {
		this.vehicleArrivalTime = vehicleArrivalTime;
	}
	public double getStartKm() {
		return startKm;
	}
	public void setStartKm(double startKm) {
		this.startKm = startKm;
	}
	public double getEndKm() {
		return endKm;
	}
	public void setEndKm(double endKm) {
		this.endKm = endKm;
	}
	public double getActKm() {
		return actKm;
	}
	public void setActKm(double actKm) {
		this.actKm = actKm;
	}
	public int getNoOfHU() {
		return noOfHU;
	}
	public void setNoOfHU(int noOfHU) {
		this.noOfHU = noOfHU;
	}
	public Date getVehicleReportingTime() {
		return vehicleReportingTime;
	}
	public void setVehicleReportingTime(Date vehicleReportingTime) {
		this.vehicleReportingTime = vehicleReportingTime;
	}
	public String getAttendanceTime() {
		return attendanceTime;
	}
	public void setAttendanceTime(String attendanceTime) {
		this.attendanceTime = attendanceTime;
	}
	public double getKm() {
		return km;
	}
	public void setKm(double km) {
		this.km = km;
	}
	public double getTat() {
		return tat;
	}
	public void setTat(double tat) {
		this.tat = tat;
	}


}
