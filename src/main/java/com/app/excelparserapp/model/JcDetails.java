package com.app.excelparserapp.model;


public class JcDetails {


	private long jcId;
	private String jcName;
	private String jcCode;
	
	public long getJcId() {
		return jcId;
	}
	public void setJcId(long jcId) {
		this.jcId = jcId;
	}
	public String getJcName() {
		return jcName;
	}
	public void setJcName(String jcName) {
		this.jcName = jcName;
	}
	public String getJcCode() {
		return jcCode;
	}
	public void setJcCode(String jcCode) {
		this.jcCode = jcCode;
	}

	

}
