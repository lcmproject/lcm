package com.app.excelparserapp.model;

public class TripDetails {

	private long tripId;
	private String tripPlanCode;
	private String tripPlanName;
	private String tripNumber;
	private VehicleDetails vehicleDetails;

	
	public VehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}
	public void setVehicleDetails(VehicleDetails vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}
	public long getTripId() {
		return tripId;
	}
	public void setTripId(long tripId) {
		this.tripId = tripId;
	}

	public String getTripPlanCode() {
		return tripPlanCode;
	}
	public void setTripPlanCode(String tripPlanCode) {
		this.tripPlanCode = tripPlanCode;
	}
	public String getTripPlanName() {
		return tripPlanName;
	}
	public void setTripPlanName(String tripPlanName) {
		this.tripPlanName = tripPlanName;
	}
	public String getTripNumber() {
		return tripNumber;
	}
	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}

	

}
