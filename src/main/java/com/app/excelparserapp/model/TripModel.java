package com.app.excelparserapp.model;

import java.util.Date;

public class TripModel {
	
	private String tripPlanCode;
	private String tripPlanName;
	private String vendorName;
	private String vehicleType;
	private String vehicleNumber;
	private long tripNumber;
	private String tripStartDate;
	//private Date tripstartTime;
	private String tripStatus;
	private String consignorName;
	private String consigneeName;
	private String lrType;
	private double lrNumber;
	private double startKm;
	private double endKm;
	private double actualKm;
	private double numberOfHU;
	private String lrDate;
	private String vehicleArrivalDate;
	//private Date vehicleArrivalTime;
	private String tripEndDate;
	//private Date tripEndTime;
	private String vehicleReportingTime;
	private double attendance;
	private double trip;
	private double Km;
	private String tAT;
	private double otCharges;
	private double tollCharges;
	private double loadUnloadCharges;
	private double otherCharges;
	private String remarks;
	public String getTripPlanCode() {
		return tripPlanCode;
	}
	public void setTripPlanCode(String tripPlanCode) {
		this.tripPlanCode = tripPlanCode;
	}
	public String getTripPlanName() {
		return tripPlanName;
	}
	public void setTripPlanName(String tripPlanName) {
		this.tripPlanName = tripPlanName;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public long getTripNumber() {
		return tripNumber;
	}
	public void setTripNumber(long tripNumber) {
		this.tripNumber = tripNumber;
	}
	public String getTripStartDate() {
		return tripStartDate;
	}
	public void setTripStartDate(String tripStartDate) {
		this.tripStartDate = tripStartDate;
	}
	public String getTripStatus() {
		return tripStatus;
	}
	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}
	public String getConsignorName() {
		return consignorName;
	}
	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	public String getLrType() {
		return lrType;
	}
	public void setLrType(String lrType) {
		this.lrType = lrType;
	}
	public double getLrNumber() {
		return lrNumber;
	}
	public void setLrNumber(double lrNumber) {
		this.lrNumber = lrNumber;
	}
	public double getStartKm() {
		return startKm;
	}
	public void setStartKm(double startKm) {
		this.startKm = startKm;
	}
	public double getEndKm() {
		return endKm;
	}
	public void setEndKm(double endKm) {
		this.endKm = endKm;
	}
	public double getActualKm() {
		return actualKm;
	}
	public void setActualKm(double actualKm) {
		this.actualKm = actualKm;
	}
	public double getNumberOfHU() {
		return numberOfHU;
	}
	public void setNumberOfHU(double numberOfHU) {
		this.numberOfHU = numberOfHU;
	}
	public String getLrDate() {
		return lrDate;
	}
	public void setLrDate(String lrDate) {
		this.lrDate = lrDate;
	}
	public String getVehicleArrivalDate() {
		return vehicleArrivalDate;
	}
	public void setVehicleArrivalDate(String vehicleArrivalDate) {
		this.vehicleArrivalDate = vehicleArrivalDate;
	}
	public String getTripEndDate() {
		return tripEndDate;
	}
	public void setTripEndDate(String tripEndDate) {
		this.tripEndDate = tripEndDate;
	}
	public String getVehicleReportingTime() {
		return vehicleReportingTime;
	}
	public void setVehicleReportingTime(String vehicleReportingTime) {
		this.vehicleReportingTime = vehicleReportingTime;
	}
	public double getAttendance() {
		return attendance;
	}
	public void setAttendance(double attendance) {
		this.attendance = attendance;
	}
	public double getTrip() {
		return trip;
	}
	public void setTrip(double trip) {
		this.trip = trip;
	}
	public double getKm() {
		return Km;
	}
	public void setKm(double km) {
		Km = km;
	}
	public String gettAT() {
		return tAT;
	}
	public void settAT(String tAT) {
		this.tAT = tAT;
	}
	public double getOtCharges() {
		return otCharges;
	}
	public void setOtCharges(double otCharges) {
		this.otCharges = otCharges;
	}
	public double getTollCharges() {
		return tollCharges;
	}
	public void setTollCharges(double tollCharges) {
		this.tollCharges = tollCharges;
	}
	public double getLoadUnloadCharges() {
		return loadUnloadCharges;
	}
	public void setLoadUnloadCharges(double loadUnloadCharges) {
		this.loadUnloadCharges = loadUnloadCharges;
	}
	public double getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
