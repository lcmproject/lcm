package com.app.excelparserapp.model;

import java.util.Date;


public class TripTransactions {


	private TripDetails tripDetails;
	private VehicleDetails vehicleDetails;
	private ConsignorDetails consignorDetails;
	private LrDetails lrDetails;
	private Date tripStartTime;
	private String tripStatus;
	
	public TripDetails getTripDetails() {
		return tripDetails;
	}
	public void setTripDetails(TripDetails tripDetails) {
		this.tripDetails = tripDetails;
	}
	public VehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}
	public void setVehicleDetails(VehicleDetails vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}
	public ConsignorDetails getConsignorDetails() {
		return consignorDetails;
	}
	public void setConsignorDetails(ConsignorDetails consignorDetails) {
		this.consignorDetails = consignorDetails;
	}
	public LrDetails getLrDetails() {
		return lrDetails;
	}
	public void setLrDetails(LrDetails lrDetails) {
		this.lrDetails = lrDetails;
	}
	public Date getTripStartTime() {
		return tripStartTime;
	}
	public void setTripStartTime(Date tripStartTime) {
		this.tripStartTime = tripStartTime;
	}
	public String getTripStatus() {
		return tripStatus;
	}
	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}

}
