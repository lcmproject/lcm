package com.app.excelparserapp.model;

import java.util.Date;

public class LrDetails {

	private long lrId;
	private String lrType;
	private String lrNo;
	private Date lrDate;

	public long getLrId() {
		return lrId;
	}

	public void setLrId(long lrId) {
		this.lrId = lrId;
	}

	public String getLrType() {
		return lrType;
	}

	public void setLrType(String lrType) {
		this.lrType = lrType;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public Date getLrDate() {
		return lrDate;
	}

	public void setLrDate(Date lrDate) {
		this.lrDate = lrDate;
	}

}
