package com.app.excelparserapp.model;


public class AccountDetails {


	private long accountId;
	private JcDetails jcDetails;
	private VehicleDetails vehicleDetails;
	private int calenderDays;
	private int presentDays;
	private int trips;
	private long km;
	private double otherCharges;
	private double fixedCosts;
	private double varCosts;
	private double totalCosts;
	
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public JcDetails getJcDetails() {
		return jcDetails;
	}
	public void setJcDetails(JcDetails jcDetails) {
		this.jcDetails = jcDetails;
	}
	public VehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}
	public void setVehicleDetails(VehicleDetails vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}
	public int getCalenderDays() {
		return calenderDays;
	}
	public void setCalenderDays(int calenderDays) {
		this.calenderDays = calenderDays;
	}
	public int getPresentDays() {
		return presentDays;
	}
	public void setPresentDays(int presentDays) {
		this.presentDays = presentDays;
	}
	public int getTrips() {
		return trips;
	}
	public void setTrips(int trips) {
		this.trips = trips;
	}
	public long getKm() {
		return km;
	}
	public void setKm(long km) {
		this.km = km;
	}
	public double getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}
	public double getFixedCosts() {
		return fixedCosts;
	}
	public void setFixedCosts(double fixedCosts) {
		this.fixedCosts = fixedCosts;
	}
	public double getVarCosts() {
		return varCosts;
	}
	public void setVarCosts(double varCosts) {
		this.varCosts = varCosts;
	}
	public double getTotalCosts() {
		return totalCosts;
	}
	public void setTotalCosts(double totalCosts) {
		this.totalCosts = totalCosts;
	}
	
	
}
