package com.app.excelparserapp.model;

public class ConsignorDetails {

	private long consignorId;
	private String consignorName;
	private String consigneeName;
	
	public long getConsignorId() {
		return consignorId;
	}
	public void setConsignorId(long consignorId) {
		this.consignorId = consignorId;
	}
	public String getConsignorName() {
		return consignorName;
	}
	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	
}
