package com.app.excelparserapp.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;

import com.app.excelparserapp.model.TripModel;
import com.app.excelparserapp.util.ExcelToModelMapper;
import com.google.gson.Gson;

@Service
public class ExcelParser {
	
	private boolean flag = true;
	private boolean flagdata = false;
	
	public String fromExcel(String srcExcel) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(new File(srcExcel));
		List<List<String>> values = new ArrayList();
		List<TripModel> modelList = new ArrayList();
		workbook.forEach(sheet -> {
			
			sheet.forEach(row -> {
				if(!this.flag){
					List<String> data = new ArrayList();
					this.flagdata = false;
					row.forEach(cell -> {
						getCellValue(cell,data);
					});
					if(this.flagdata){
						values.add(data);
						System.out.println(data);
					}
						
				}
				this.flag = false;
				
			});
			System.out.println(values.size());
		});
		for (List<String> value : values) {
			TripModel model = ExcelToModelMapper.doMapping(value);
			modelList.add(model);
		}
		return new Gson().toJson(modelList);
	}

	private void getCellValue(Cell cell, List<String> data) {
		String value = null;
		switch (cell.getCellTypeEnum()) {

		case STRING:
			value = cell.getRichStringCellValue().getString();
			data.add(value);
			this.flagdata = true;
			break;

		case BLANK:
			value = "";
			data.add(value);
			break;
		default:
			value = "";
			data.add(value);
		}

	}
}
