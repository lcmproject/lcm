package com.app.excelparserapp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.app.excelparserapp.service.ExcelParser;

@SpringBootApplication
public class ExcelParserAppApplication {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		//SpringApplication.run(ExcelParserAppApplication.class, args);
		String path = "./src/main/resources/input/RELIANCE JIO TRANPORTATION LOG.xlsx";
		ExcelParser excelParser = new ExcelParser();
		String data = excelParser.fromExcel(path);
		// BufferedWriter writer = new BufferedWriter(new FileWriter("E:\\myproject\\json\\trips.json"));
		// writer.write(data);
		System.out.println(data);
		
		
	}
	
	private static void writeInFile(String data, int noOfLines){
		File file = new File("/E:\\myproject\\json\\trips.json");
        FileWriter fr = null;
        BufferedWriter br = null;
        String dataWithNewLine=data+System.getProperty("line.separator");
        try{
            fr = new FileWriter(file);
            br = new BufferedWriter(fr);
            for(int i = noOfLines; i>0; i--){
                br.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
}
