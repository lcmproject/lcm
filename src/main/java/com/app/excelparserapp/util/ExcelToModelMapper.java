package com.app.excelparserapp.util;

import java.util.List;

import org.springframework.util.StringUtils;

import com.app.excelparserapp.model.TripModel;

public class ExcelToModelMapper {
	
	public static TripModel doMapping(List<String> data){
		TripModel model = new TripModel();
		if(data != null && data.size()>0){
			
			model.setTripPlanCode(validate(data,0));
			model.setTripPlanName(validate(data,1));
			model.setVendorName(validate(data,2));
			model.setVehicleType(validate(data,3));
			model.setVehicleNumber(validate(data,4));
			model.setTripNumber(StringUtils.isEmpty(validate(data,5)) || validate(data,5) instanceof String ?0:Long.parseLong(validate(data,5)));
			model.setTripStartDate(validate(data,6)+" "+validate(data,7));
			model.setTripStatus(validate(data,8));
			model.setConsignorName(validate(data,9));
			model.setConsigneeName(validate(data,10));
			model.setLrType(validate(data,11));
			model.setLrNumber(StringUtils.isEmpty(validate(data,12))|| validate(data,12) instanceof String ?0:Double.parseDouble(validate(data,12)));
			model.setStartKm(StringUtils.isEmpty(validate(data,13)) || validate(data,13) instanceof String ?0:Double.parseDouble(validate(data,13)));
			model.setEndKm(StringUtils.isEmpty(validate(data,14)) || validate(data,14) instanceof String ?0:Double.parseDouble(validate(data,14)));
			model.setActualKm(StringUtils.isEmpty(validate(data,15))|| validate(data,15) instanceof String ?0:Double.parseDouble(validate(data,15)));
			model.setNumberOfHU(StringUtils.isEmpty(validate(data,16)) || validate(data,16) instanceof String ?0:Double.parseDouble(validate(data,16)));
			model.setLrDate(validate(data,17));
			model.setVehicleArrivalDate(validate(data,18)+" "+validate(data,19));
			model.setTripEndDate(validate(data,21)+" "+validate(data,20));
			model.setVehicleReportingTime(validate(data,22));
			model.setAttendance(StringUtils.isEmpty(validate(data,23)) || validate(data,23) instanceof String ?0:Double.parseDouble(validate(data,23)));
			model.setTrip(StringUtils.isEmpty(validate(data,24)) || validate(data,24) instanceof String ?0:Double.parseDouble(validate(data,24)));
			model.setKm(StringUtils.isEmpty(validate(data,25)) || validate(data,25) instanceof String ?0:Double.parseDouble(validate(data,25)));
			model.settAT(validate(data,26));
			model.setOtCharges(StringUtils.isEmpty(validate(data,27)) || validate(data,27) instanceof String ?0:Double.parseDouble(validate(data,27)));
			model.setTollCharges(StringUtils.isEmpty(validate(data,28)) || validate(data,28) instanceof String ?0:Double.parseDouble(validate(data,28)));
			model.setLoadUnloadCharges(StringUtils.isEmpty(validate(data,29)) || validate(data,29) instanceof String ?0:Double.parseDouble(validate(data,29)));
			model.setOtherCharges(StringUtils.isEmpty(validate(data,30)) || validate(data,30) instanceof String ?0:Double.parseDouble(validate(data,30)));
			model.setRemarks(validate(data,31));
			
		}
		return model;
	}

	private static String validate(List<String> data, int i) {
		try{
		return data.get(i);
		}catch(Exception e){
		return null;
		}
	}
}
