CREATE DATABASE IF NOT EXISTS lcm;

USE lcm;

CREATE TABLE  IF NOT EXISTS VENDOR(vendor_id int not null auto_increment,vendor_name varchar(100),primary key(vendor_id));
 
CREATE TABLE  IF NOT EXISTS VEHICLE (vehicle_id int not null auto_increment,vendor_id int,vehicle_type varchar(100),
vehicle_number varchar(100),fleet_type varchar(100),vehicle_arrival_time date, primary key(vehicle_id),foreign key(vendor_id)
references VENDOR(vendor_id));

CREATE TABLE  IF NOT EXISTS TRIP(trip_id int not null auto_increment, trip_plan_code varchar(100), trip_plan_name varchar(100),
trip_no varchar(100),vehicle_id int, primary key(trip_id),foreign key(vehicle_id) references VEHICLE(vehicle_id));

CREATE TABLE  IF NOT EXISTS CONSIGNOR (consignor_id int not null auto_increment,consignor_name varchar(100),consignee_name varchar(100),primary key(consignor_id));

CREATE TABLE  IF NOT EXISTS LR(lr_id int not null auto_increment,lr_type varchar(100),lr_no varchar(100),lr_date date,
primary key(lr_id));

CREATE TABLE  IF NOT EXISTS JC(jc_id int not null auto_increment,jc_code varchar(100),jc_name varchar(100),primary key(jc_id));


CREATE TABLE  IF NOT EXISTS VEHICLE_TRANSACTION (vehicle_transaction_id int not null auto_increment,vehicle_id int,start_km int, end_km int, act_km int,
no_of_hu int, vehicle_report_time date, attendance date, km int, total_tat date ,primary key(vehicle_transaction_id),foreign key (vehicle_id) references
VEHICLE (vehicle_id)
);

CREATE TABLE  IF NOT EXISTS TRIP_TRANSACTION (trip_transaction_id int not null auto_increment,vehicle_id int,trip_code varchar(100),
consignor_id int,lr_id int, trip_start_date date, trip int, trip_status varchar(50),primary key(trip_transaction_id),
foreign key (vehicle_id) references VEHICLE (vehicle_id),
foreign key (consignor_id) references CONSIGNOR (consignor_id),
foreign key (lr_id) references LR (lr_id)
);

CREATE TABLE  IF NOT EXISTS ACCOUNT_TRANSACTION (account_transaction_id int not null auto_increment,vehicle_id int,jc_id int,
calendar_days int,present_days int, trips int, trip int, km int,other_charges double, fix_costs double,var_costs double,total_costs double
,primary key(account_transaction_id),
foreign key (vehicle_id) references VEHICLE (vehicle_id),
foreign key (jc_id) references JC (jc_id)
);




 
 